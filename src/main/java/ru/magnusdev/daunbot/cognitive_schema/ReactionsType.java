//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.10.26 at 11:01:55 PM MSK 
//


package ru.magnusdev.daunbot.cognitive_schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReactionsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReactionsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="sticker" type="{http://www.daunbot.magnusdev.ru/cognitive_templates}StickerType"/&gt;
 *         &lt;element name="text" type="{http://www.daunbot.magnusdev.ru/cognitive_templates}TextType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReactionsType", propOrder = {
    "stickerAndText"
})
public class ReactionsType {

    @XmlElements({
        @XmlElement(name = "sticker", type = StickerType.class),
        @XmlElement(name = "text", type = TextType.class)
    })
    protected List<Object> stickerAndText;

    /**
     * Gets the value of the stickerAndText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stickerAndText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStickerAndText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StickerType }
     * {@link TextType }
     * 
     * 
     */
    public List<Object> getStickerAndText() {
        if (stickerAndText == null) {
            stickerAndText = new ArrayList<Object>();
        }
        return this.stickerAndText;
    }

}
