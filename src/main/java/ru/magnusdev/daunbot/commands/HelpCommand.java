package ru.magnusdev.daunbot.commands;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;
import org.telegram.telegrambots.bots.commands.ICommandRegistry;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

public class HelpCommand extends BotCommand {

	private static final String LOGTAG = "DaunBotHelpCommand";

	public HelpCommand(ICommandRegistry commandRegistry) {
		super("help", "Get all the commands this bot provides");
	}

	@Override
	public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
		StringBuilder helpMessageBuilder = new StringBuilder("<b>Help</b>\n");
		String userName = user.getFirstName() + " " + user.getLastName();
		helpMessageBuilder.append(userName + ", тебе уже ничего не поможет :[");
		SendMessage helpMessage = new SendMessage();
		helpMessage.setChatId(chat.getId().toString());
		helpMessage.enableHtml(true);
		helpMessage.setText(helpMessageBuilder.toString());
		try {
			absSender.sendMessage(helpMessage);
		} catch (TelegramApiException e) {
			BotLogger.error(LOGTAG, e);
		}
	}
}
