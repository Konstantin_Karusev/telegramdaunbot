package ru.magnusdev.daunbot.commands;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

public class StartCommand extends BotCommand {

	private static final String LOGTAG = "DaunBotStartCommand";

	public StartCommand() {
		super("start", "With this command you can start the Bot");
	}

	@Override
	public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
		StringBuilder messageBuilder = new StringBuilder();
		String userName = user.getFirstName() + " " + user.getLastName();
		messageBuilder.append("Сам себе очкой постартуй, " + userName);
		SendMessage answer = new SendMessage();
		answer.setChatId(chat.getId().toString());
		answer.setText(messageBuilder.toString());
		try {
			absSender.sendMessage(answer);
		} catch (TelegramApiException e) {
			BotLogger.error(LOGTAG, e);
		}
	}

}
