package ru.magnusdev.daunbot.db.controller;

import org.hibernate.Session;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.User;

import ru.magnusdev.daunbot.db.model.TDbtChat;
import ru.magnusdev.daunbot.db.model.TDbtUser;
import ru.magnusdev.daunbot.utils.HibernateSessionFactory;

public class DaunbotController {

	private static HibernateSessionFactory sessionFactoryHolder = HibernateSessionFactory.getInstance();

	public void persistRawMessage(Message message) throws Exception {
		User user = message.getFrom();
		Session session = sessionFactoryHolder.getSessionFactory().openSession();
		TDbtChat chatObj = DaunbotDAO.getOrAddChat(session, message.getChatId().toString());
		TDbtUser userObj =
				DaunbotDAO.getOrAddUser(session, user.getId().toString(), user.getUserName(), user.getFirstName(), user.getLastName());
		DaunbotDAO.addRawMessage(session, chatObj, message.getText(), userObj);
		session.close();
	}

}
