package ru.magnusdev.daunbot.db.controller;

import java.sql.Timestamp;

import org.hibernate.Query;
import org.hibernate.Session;

import ru.magnusdev.daunbot.db.model.TDbtChat;
import ru.magnusdev.daunbot.db.model.TDbtRawMessage;
import ru.magnusdev.daunbot.db.model.TDbtUser;

public class DaunbotDAO {

	public static void addRawMessage(Session session, TDbtChat chat, String message, TDbtUser user) {
		TDbtRawMessage rawMessage = new TDbtRawMessage();
		rawMessage.setMessage(message);
		rawMessage.setTDbtUser(user);
		rawMessage.setTimestamp(new Timestamp(System.currentTimeMillis()));
		rawMessage.setTDbtChat(chat);
		session.beginTransaction();
		session.save(rawMessage);
		session.getTransaction().commit();
		session.flush();
	}

	public static TDbtChat getOrAddChat(Session session, String chatId) {
		Query getByChatIdQuery = session.getNamedQuery("TDbtChat.findChatById");
		getByChatIdQuery.setParameter(0, chatId);
		TDbtChat chatObj = (TDbtChat) getByChatIdQuery.uniqueResult();
		if (chatObj == null) {
			chatObj = new TDbtChat();
			chatObj.setChatId(chatId);
			session.beginTransaction();
			session.save(chatObj);
			session.getTransaction().commit();
			session.flush();
		}
		return chatObj;
	}

	public static TDbtUser getOrAddUser(Session session, String userId, String username, String firstname, String surname) {
		Query getByUserIdQuery = session.getNamedQuery("TDbtUser.findUserById");
		getByUserIdQuery.setParameter(0, userId);
		TDbtUser userObj = (TDbtUser) getByUserIdQuery.uniqueResult();
		boolean saveFlag = false;
		if (userObj == null) {
			userObj = new TDbtUser();
			userObj.setUserId(userId);
			userObj.setFirstName(firstname);
			userObj.setSurname(surname);
			userObj.setUsername(username);
			saveFlag = true;
		} else {
			if (userObj.getUsername() != null && username != null && !userObj.getUsername().equals(username)) {
				userObj.setUsername(username);
				saveFlag = true;
			}
			if (userObj.getFirstName() != null && firstname != null && !userObj.getFirstName().equals(firstname)) {
				userObj.setFirstName(firstname);
				saveFlag = true;
			}
			if (userObj.getSurname() != null && surname != null && !userObj.getSurname().equals(surname)) {
				userObj.setSurname(surname);
				saveFlag = true;
			}
		}
		if (saveFlag) {
			session.beginTransaction();
			session.save(userObj);
			session.getTransaction().commit();
			session.flush();
		}
		return userObj;
	}
}
