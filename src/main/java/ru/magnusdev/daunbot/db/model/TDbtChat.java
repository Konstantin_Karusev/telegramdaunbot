package ru.magnusdev.daunbot.db.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the T_DBT_CHAT database table.
 */
@Entity
@Table(name = "T_DBT_CHAT")
@NamedQueries({ @NamedQuery(name = "TDbtChat.findAll", query = "SELECT t FROM TDbtChat t"),
		@NamedQuery(name = "TDbtChat.findChatById", query = "SELECT t FROM TDbtChat t WHERE chatId = ?")
})
public class TDbtChat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "CHAT_ID", length = 255)
	private String chatId;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DbtChatIdSequence")
	@SequenceGenerator(name = "DbtChatIdSequence", sequenceName = "S_ID_DBT_CHAT", allocationSize = 1)
	@Column(unique = true, nullable = false)
	private int id;

	//bi-directional many-to-one association to TDbtDictionary
	@OneToMany(mappedBy = "TDbtChat")
	private List<TDbtDictionary> TDbtDictionaries;

	//bi-directional many-to-one association to TDbtRawMessage
	@OneToMany(mappedBy = "TDbtChat")
	private List<TDbtRawMessage> TDbtRawMessages;

	public TDbtChat() {
	}

	public TDbtDictionary addTDbtDictionary(TDbtDictionary TDbtDictionary) {
		getTDbtDictionaries().add(TDbtDictionary);
		TDbtDictionary.setTDbtChat(this);

		return TDbtDictionary;
	}

	public TDbtRawMessage addTDbtRawMessage(TDbtRawMessage TDbtRawMessage) {
		getTDbtRawMessages().add(TDbtRawMessage);
		TDbtRawMessage.setTDbtChat(this);

		return TDbtRawMessage;
	}

	public String getChatId() {
		return chatId;
	}

	public int getId() {
		return id;
	}

	public List<TDbtDictionary> getTDbtDictionaries() {
		return TDbtDictionaries;
	}

	public List<TDbtRawMessage> getTDbtRawMessages() {
		return TDbtRawMessages;
	}

	public TDbtDictionary removeTDbtDictionary(TDbtDictionary TDbtDictionary) {
		getTDbtDictionaries().remove(TDbtDictionary);
		TDbtDictionary.setTDbtChat(null);

		return TDbtDictionary;
	}

	public TDbtRawMessage removeTDbtRawMessage(TDbtRawMessage TDbtRawMessage) {
		getTDbtRawMessages().remove(TDbtRawMessage);
		TDbtRawMessage.setTDbtChat(null);

		return TDbtRawMessage;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTDbtDictionaries(List<TDbtDictionary> TDbtDictionaries) {
		this.TDbtDictionaries = TDbtDictionaries;
	}

	public void setTDbtRawMessages(List<TDbtRawMessage> TDbtRawMessages) {
		this.TDbtRawMessages = TDbtRawMessages;
	}

}
