package ru.magnusdev.daunbot.db.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the T_DBT_DICTIONARY database table.
 */
@Entity
@Table(name = "T_DBT_DICTIONARY")
@NamedQuery(name = "TDbtDictionary.findAll", query = "SELECT t FROM TDbtDictionary t")
public class TDbtDictionary implements Serializable {
	private static final long serialVersionUID = 1L;

	private int counter;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DbtDictionaryIdSequence")
	@SequenceGenerator(name = "DbtDictionaryIdSequence", sequenceName = "S_ID_DBT_DICTIONARY", allocationSize = 1)
	@Column(unique = true, nullable = false)
	private int id;

	//bi-directional many-to-one association to TDbtChat
	@ManyToOne
	@JoinColumn(name = "CHAT_ID")
	private TDbtChat TDbtChat;

	@Column(length = 255)
	private String word;

	public TDbtDictionary() {
	}

	public int getCounter() {
		return counter;
	}

	public int getId() {
		return id;
	}

	public TDbtChat getTDbtChat() {
		return TDbtChat;
	}

	public String getWord() {
		return word;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTDbtChat(TDbtChat TDbtChat) {
		this.TDbtChat = TDbtChat;
	}

	public void setWord(String word) {
		this.word = word;
	}

}
