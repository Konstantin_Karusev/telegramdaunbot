package ru.magnusdev.daunbot.db.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the T_DBT_RAW_MESSAGE database table.
 */
@Entity
@Table(name = "T_DBT_RAW_MESSAGE")
@NamedQuery(name = "TDbtRawMessage.findAll", query = "SELECT t FROM TDbtRawMessage t")
public class TDbtRawMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DbtRawMessageIdSequence")
	@SequenceGenerator(name = "DbtRawMessageIdSequence", sequenceName = "S_ID_DBT_RAW_MESSAGE", allocationSize = 1)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(length = 5000)
	private String message;

	@ManyToOne
	@JoinColumn(name = "CHAT_ID")
	private TDbtChat TDbtChat;

	//bi-directional many-to-one association to TDbtChat
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private TDbtUser TDbtUser;

	//bi-directional many-to-one association to TDbtUser
	private Timestamp timestamp;

	public TDbtRawMessage() {
	}

	public int getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}

	public TDbtChat getTDbtChat() {
		return TDbtChat;
	}

	public TDbtUser getTDbtUser() {
		return TDbtUser;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setTDbtChat(TDbtChat TDbtChat) {
		this.TDbtChat = TDbtChat;
	}

	public void setTDbtUser(TDbtUser TDbtUser) {
		this.TDbtUser = TDbtUser;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

}
