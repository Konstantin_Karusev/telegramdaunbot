package ru.magnusdev.daunbot.db.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the T_DBT_USER database table.
 */
@Entity
@Table(name = "T_DBT_USER")
@NamedQueries({ @NamedQuery(name = "TDbtUser.findAll", query = "SELECT t FROM TDbtUser t"),
		@NamedQuery(name = "TDbtUser.findUserById", query = "SELECT t FROM TDbtUser t WHERE userId = ?") })
public class TDbtUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "FIRST_NAME", length = 64)
	private String firstName;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DbtUserIdSequence")
	@SequenceGenerator(name = "DbtUserIdSequence", sequenceName = "S_ID_DBT_USER", allocationSize = 1)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(length = 64)
	private String surname;

	//bi-directional many-to-one association to TDbtRawMessage
	@OneToMany(mappedBy = "TDbtUser")
	private List<TDbtRawMessage> TDbtRawMessages;

	@Column(name = "USER_ID", length = 64)
	private String userId;

	@Column(length = 128)
	private String username;

	public TDbtUser() {
	}

	public TDbtRawMessage addTDbtRawMessage(TDbtRawMessage TDbtRawMessage) {
		getTDbtRawMessages().add(TDbtRawMessage);
		TDbtRawMessage.setTDbtUser(this);

		return TDbtRawMessage;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getId() {
		return id;
	}

	public String getSurname() {
		return surname;
	}

	public List<TDbtRawMessage> getTDbtRawMessages() {
		return TDbtRawMessages;
	}

	public String getUserId() {
		return userId;
	}

	public String getUsername() {
		return username;
	}

	public TDbtRawMessage removeTDbtRawMessage(TDbtRawMessage TDbtRawMessage) {
		getTDbtRawMessages().remove(TDbtRawMessage);
		TDbtRawMessage.setTDbtUser(null);

		return TDbtRawMessage;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setTDbtRawMessages(List<TDbtRawMessage> TDbtRawMessages) {
		this.TDbtRawMessages = TDbtRawMessages;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
