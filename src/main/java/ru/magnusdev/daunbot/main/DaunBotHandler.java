package ru.magnusdev.daunbot.main;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendSticker;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

import ru.magnusdev.daunbot.cognitive_schema.StickerType;
import ru.magnusdev.daunbot.cognitive_schema.TextType;
import ru.magnusdev.daunbot.commands.HelpCommand;
import ru.magnusdev.daunbot.commands.StartCommand;
import ru.magnusdev.daunbot.db.controller.DaunbotController;
import ru.magnusdev.daunbot.utils.Context;
import ru.magnusdev.daunbot.utils.TextProcessor;

public class DaunBotHandler extends TelegramLongPollingCommandBot {

	private static Context context = Context.getInstance();

	private DaunbotController controller;

	private Integer ignoreDealy;

	private final String LOGTAG = "DaunBotHandler";

	private Random rand;

	private TextProcessor textProcessor;

	public DaunBotHandler() {
		try {
			BotLogger.info(LOGTAG, "Starting handler");
			register(new StartCommand());
			HelpCommand helpCommand = new HelpCommand(this);
			register(helpCommand);

			registerDefaultAction((absSender, message) -> {
				SendMessage commandUnknownMessage = new SendMessage();
				commandUnknownMessage.setChatId(message.getChatId().toString());
				commandUnknownMessage.setText("Я не знаю такой команды. Я же даун.");
				try {
					absSender.sendMessage(commandUnknownMessage);
				} catch (TelegramApiException e) {
					BotLogger.error(LOGTAG, e);
				}
			});
			BotLogger.info(LOGTAG, "Commands registered");
			String spellCheckerClassName = context.getProperty("input.spellchecker");
			String normalizerClassName = context.getProperty("input.normalizer");
			String textPreprocessorExpr = context.getProperty("input.escape.expr");
			String synonymizerClassName = context.getProperty("input.synonymizer");
			textProcessor = new TextProcessor(spellCheckerClassName, normalizerClassName, synonymizerClassName,
					textPreprocessorExpr);
			ignoreDealy = Integer.parseInt(context.getProperty("bot.delay.ignore"));
			rand = new Random();
			controller = new DaunbotController();
			BotLogger.info(LOGTAG, "Handler started");
		} catch (Exception e) {
			BotLogger.error(LOGTAG, e);
		}
	}

	@Override
	public boolean filter(Message message) {
		Integer msgDate = message.getDate();
		Integer currentTime = (int) (System.currentTimeMillis() / 1000);
		return (currentTime - msgDate > ignoreDealy);
	}

	@Override
	public String getBotToken() {
		return context.getProperty("bot.token");
	}

	@Override
	public String getBotUsername() {
		return context.getProperty("bot.username");
	}

	@Override
	public void processNonCommandUpdate(Update update) {
		try {
			if (update.hasMessage() && !filter(update.getMessage())) {
				Message message = update.getMessage();
				Set<String> stickersToSend = new HashSet<>();
				if (message.hasText()) {
					controller.persistRawMessage(message);
					String messageText = message.getText().toLowerCase();
					if (!context.getComplexTextMarkerMap().isEmpty()) {
						for (Entry<Pattern, String> patternEntry : context.getComplexTextMarkerMap().entrySet()) {
							Matcher m = patternEntry.getKey().matcher(messageText);
							if (m.matches()) {
								m.replaceFirst("");
								String templateName = patternEntry.getValue();
								stickersToSend.add(templateName);
							}
						}
					}
					Map<String, Collection<String>> result = textProcessor.process(messageText);
					for (Entry<String, Collection<String>> entry : result.entrySet()) {
						String templateName = null;
						if (context.getLiteralMarkers().contains(entry.getKey())) {
							templateName = context.getSimpleTextMarkerMap().get(entry.getKey());
							if (templateName != null) {
								stickersToSend.add(templateName);
								break;
							}
						}
						for (String str : entry.getValue()) {
							templateName = context.getSimpleTextMarkerMap().get(str);
							if (templateName != null) {
								stickersToSend.add(templateName);
								break;
							}
						}
					}
				} else if (message.getSticker() != null) {

				}
				for (String sticker : stickersToSend) {
					sendResponse(sticker, message.getChatId().toString());
				}
			}
		} catch (Exception e) {
			BotLogger.error(LOGTAG, e);
		}
	}

	private void sendResponse(String templateName, String chatId) {
		if (templateName != null) {
			try {
				List<Object> responses = context.getResponseMap().get(templateName);
				Object response = responses.get(rand.nextInt(responses.size()));
				if (response instanceof TextType) {
					SendMessage sendMessageRequest = new SendMessage();
					sendMessageRequest.setChatId(chatId);
					sendMessageRequest.setText(((TextType) response).getValue());
					sendMessage(sendMessageRequest);
				} else if (response instanceof StickerType) {
					SendSticker sendStickerRequest = new SendSticker();
					sendStickerRequest.setChatId(chatId);
					sendStickerRequest.setSticker(((StickerType) response).getValue());
					sendSticker(sendStickerRequest);
				}

			} catch (Exception e) {
				BotLogger.error(LOGTAG, e);
			}
			return;
		}
	}

}
