package ru.magnusdev.daunbot.main;

import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

public class Main {

	public static void main(String[] args) {
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			telegramBotsApi.registerBot(new DaunBotHandler());
		} catch (TelegramApiException e) {
			BotLogger.error("TestTag", e);
		}
	}

}
