package ru.magnusdev.daunbot.normalizer;

import java.util.Collection;
import java.util.Map;

public interface NormalizationAdapter {

	public Map<String, Collection<String>> normalize(Collection<String> words);

}
