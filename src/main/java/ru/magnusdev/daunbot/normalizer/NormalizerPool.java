package ru.magnusdev.daunbot.normalizer;

import java.util.Collection;
import java.util.Map;

import ru.magnusdev.daunbot.utils.ObjectPool;

public class NormalizerPool {

	private ObjectPool<NormalizationAdapter> pool;

	public NormalizerPool(Class<NormalizationAdapter> type) {
		pool = new ObjectPool<NormalizationAdapter>(1, 5, 20) {

			@Override
			protected NormalizationAdapter createObject() {
				try {
					return type.newInstance();
				} catch (Exception e) {
					return null;
				}
			}

		};
	}

	public Map<String, Collection<String>> normalizeWords(Collection<String> words2normalize) throws Exception {
		NormalizationAdapter obj = pool.borrowObject();
		Map<String, Collection<String>> result = obj.normalize(words2normalize);
		pool.returnObject(obj);
		return result;
	}

}
