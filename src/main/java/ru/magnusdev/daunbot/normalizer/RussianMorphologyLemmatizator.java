package ru.magnusdev.daunbot.normalizer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.morphology.russian.RussianMorphology;

import ru.magnusdev.daunbot.utils.Context;

public class RussianMorphologyLemmatizator implements NormalizationAdapter {

	private int limit;
	private RussianMorphology morph;

	public RussianMorphologyLemmatizator() throws IOException {
		morph = new RussianMorphology();
		limit = Integer.parseInt(Context.getInstance().getProperty("input.normalizer.limit"));
	}

	@Override
	public Map<String, Collection<String>> normalize(Collection<String> words) {
		Map<String, Collection<String>> result = new LinkedHashMap<>();
		for (String word : words) {
			if (word != null && !word.isEmpty()) {
				Collection<String> newSet = new HashSet<>(Arrays.asList(word));
				List<String> res = morph.getNormalForms(word);
				newSet.addAll(res.subList(0, limit < res.size() ? limit : res.size()));
				result.put(word, newSet);
			}
		}
		return result;
	}

}
