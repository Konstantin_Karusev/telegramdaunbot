package ru.magnusdev.daunbot.normalizer;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import org.tartarus.snowball.ext.RussianStemmer;

public class SnowBallStemmer implements NormalizationAdapter {

	private RussianStemmer stemmer;

	public SnowBallStemmer() {
		stemmer = new RussianStemmer();
	}

	@Override
	public Map<String, Collection<String>> normalize(Collection<String> words) {
		Map<String, Collection<String>> result = new LinkedHashMap<>();
		for (String word : words) {
			Collection<String> newSet = new HashSet<>();
			stemmer.setCurrent(word);
			stemmer.stem();
			newSet.add(stemmer.getCurrent());
			result.put(word, newSet);
		}
		return result;
	}

}
