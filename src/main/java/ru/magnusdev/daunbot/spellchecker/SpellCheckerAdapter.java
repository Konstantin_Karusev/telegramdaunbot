package ru.magnusdev.daunbot.spellchecker;

import java.util.Collection;
import java.util.Map;

public interface SpellCheckerAdapter {

	public Map<String, Collection<String>> check(Collection<String> words);

}
