package ru.magnusdev.daunbot.spellchecker;

import java.util.Collection;
import java.util.Map;

import ru.magnusdev.daunbot.utils.ObjectPool;

public class SpellCheckerPool {

	private ObjectPool<SpellCheckerAdapter> pool;

	public SpellCheckerPool(Class<SpellCheckerAdapter> type) {
		pool = new ObjectPool<SpellCheckerAdapter>(10, 20, 20) {

			@Override
			protected SpellCheckerAdapter createObject() {
				try {
					return type.newInstance();
				} catch (Exception e) {
					return null;
				}
			}

		};
	}

	public Map<String, Collection<String>> checkWords(Collection<String> words2check) throws Exception {
		SpellCheckerAdapter obj = pool.borrowObject();
		Map<String, Collection<String>> result = obj.check(words2check);
		pool.returnObject(obj);
		return result;
	}
}
