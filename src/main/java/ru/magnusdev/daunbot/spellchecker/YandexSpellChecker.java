package ru.magnusdev.daunbot.spellchecker;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import net.yandex.speller.services.spellservice.CheckTextRequest;
import net.yandex.speller.services.spellservice.CheckTextResponse;
import net.yandex.speller.services.spellservice.SpellError;
import net.yandex.speller.services.spellservice.SpellResult;
import net.yandex.speller.services.spellservice.SpellService;
import net.yandex.speller.services.spellservice.SpellServiceSoap;
import ru.magnusdev.daunbot.utils.Context;

public class YandexSpellChecker implements SpellCheckerAdapter {

	private SpellServiceSoap client;
	private int errorLimit;

	public YandexSpellChecker() {
		client = new SpellService().getSpellServiceSoap();
		errorLimit = Integer.parseInt(Context.getInstance().getProperty("input.spellchecker.limit"));
	}

	@Override
	public Map<String, Collection<String>> check(Collection<String> words) {
		CheckTextRequest request = new CheckTextRequest();
		Map<String, Collection<String>> result = new LinkedHashMap<>();
		StringBuffer sb = new StringBuffer();
		for (String word : words) {
			if (word != null && !word.isEmpty()) {
				result.put(word, new HashSet<>(Arrays.asList(word)));
				sb.append(word + " ");
			}
		}
		request.setText(sb.toString());
		request.setLang("ru");
		request.setOptions(516);
		CheckTextResponse response = client.checkText(request);
		SpellResult resultObject = response.getSpellResult();
		if (resultObject.getError() != null && !resultObject.getError().isEmpty()) {
			for (SpellError error : resultObject.getError()) {
				for (String str : error.getS().subList(0, errorLimit < error.getS().size() ? errorLimit : error.getS().size())) {
					result.get(error.getWord()).add(str.replaceAll(Context.getInstance().getProperty("input.escape.expr"), ""));
				}
			}
		}
		return result;
	}

}
