package ru.magnusdev.daunbot.synonymizer;

import java.util.Collection;
import java.util.Map;

public interface SynonymizerAdapter {

	public Map<String, Collection<String>> synonymize(Collection<String> words);

}
