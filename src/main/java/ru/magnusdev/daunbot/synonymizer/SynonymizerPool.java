package ru.magnusdev.daunbot.synonymizer;

import java.util.Collection;
import java.util.Map;

import ru.magnusdev.daunbot.utils.ObjectPool;

public class SynonymizerPool {

	private ObjectPool<SynonymizerAdapter> pool;

	public SynonymizerPool(Class<SynonymizerAdapter> type) {
		pool = new ObjectPool<SynonymizerAdapter>(10, 20, 20) {

			@Override
			protected SynonymizerAdapter createObject() {
				try {
					return type.newInstance();
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}

		};
	}

	public Map<String, Collection<String>> synonymizeWords(Collection<String> words2synonymize) throws Exception {
		SynonymizerAdapter obj = pool.borrowObject();
		Map<String, Collection<String>> result = obj.synonymize(words2synonymize);
		pool.returnObject(obj);
		return result;
	}

}
