package ru.magnusdev.daunbot.synonymizer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import ru.magnusdev.daunbot.utils.Context;

public class Word2VecSynonymizer implements SynonymizerAdapter {

	private String limit;
	private WebResource webResource;

	public Word2VecSynonymizer() throws IOException {
		ClientConfig cfg = new DefaultClientConfig();
		cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
		webResource = client.resource(Context.getInstance().getProperty("input.synonymizer.url"));
		limit = Context.getInstance().getProperty("input.synonymizer.limit");
	}

	@Override
	public Map<String, Collection<String>> synonymize(Collection<String> words) {
		Map<String, Collection<String>> result = new LinkedHashMap<>();
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		for (String word : words) {
			params.add("words", word);
		}
		params.add("limit", limit);
		ClientResponse clientResponse = webResource.path("/synonymizeWords")
				.queryParams(params)
				.get(ClientResponse.class);

		List<WordsWrapper> response = clientResponse.getEntity(new GenericType<List<WordsWrapper>>() {
		});
		for (WordsWrapper wordWrapper : response) {
			Collection<String> newSet = new HashSet<>(Arrays.asList(wordWrapper.getOriginalWord()));
			newSet.addAll(wordWrapper.getWords());
			result.put(wordWrapper.getOriginalWord(), newSet);
		}
		return result;
	}

}
