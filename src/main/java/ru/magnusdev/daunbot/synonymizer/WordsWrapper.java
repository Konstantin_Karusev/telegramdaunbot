package ru.magnusdev.daunbot.synonymizer;

import java.util.Collection;

public class WordsWrapper {

	private String originalWord;

	private Collection<String> words;

	public WordsWrapper() {

	}

	public String getOriginalWord() {
		return originalWord;
	}

	public Collection<String> getWords() {
		return words;
	}

	public void setOriginalWord(String originalWord) {
		this.originalWord = originalWord;
	}

	public void setWords(Collection<String> words) {
		this.words = words;
	}

}
