package ru.magnusdev.daunbot.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import ru.magnusdev.daunbot.cognitive_schema.CognitiveTemplateType;
import ru.magnusdev.daunbot.cognitive_schema.CognitiveTemplatesType;
import ru.magnusdev.daunbot.cognitive_schema.ComplexTextMarkerType;
import ru.magnusdev.daunbot.cognitive_schema.ObjectFactory;
import ru.magnusdev.daunbot.cognitive_schema.SimpleTextMarkerType;

public class Context {

	private volatile static Context context;
	private static Object monitor = new Object();

	public static Context getInstance() {
		if (context == null) {
			synchronized (monitor) {
				if (context == null) {
					context = new Context();
				}
			}
		}
		return context;
	}

	private Map<Pattern, String> complexTextMarkerMap = new ConcurrentHashMap<>();
	private Set<String> literalMarkers = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
	private Properties props;
	private Map<String, List<Object>> responseMap = new ConcurrentHashMap<>();
	private Map<String, String> simpleTextMarkerMap = new ConcurrentHashMap<>();

	public Context() {
		try (InputStreamReader input = new InputStreamReader(new FileInputStream("config/params.properties"), "UTF-8");) {
			File file = new File("config/cognitive_templates.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			@SuppressWarnings("unchecked")
			CognitiveTemplatesType root = ((JAXBElement<CognitiveTemplatesType>) jaxbUnmarshaller.unmarshal(file))
					.getValue();
			for (CognitiveTemplateType template : root.getCognitiveTemplate()) {
				List<Object> l = new ArrayList<>();
				for (Object reply : template.getReactions().getStickerAndText()) {
					l.add(reply);
				}
				responseMap.put(template.getName(), l);
				for (Object markerObj : template.getMarkers().getTextMarkers().getSimpleTextMarkerAndComplexTextMarker()) {
					if (markerObj instanceof SimpleTextMarkerType) {
						Boolean isLiteral = ((SimpleTextMarkerType) markerObj).isLiteral();
						String value = ((SimpleTextMarkerType) markerObj).getValue();
						if (isLiteral != null && isLiteral) {
							literalMarkers.add(value);
						}
						simpleTextMarkerMap.put(value, template.getName());
					} else if (markerObj instanceof ComplexTextMarkerType) {
						Pattern p = Pattern.compile(((ComplexTextMarkerType) markerObj).getValue());
						complexTextMarkerMap.put(p, template.getName());
					}
				}
			}
			props = new Properties();
			props.load(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<Pattern, String> getComplexTextMarkerMap() {
		return complexTextMarkerMap;
	}

	public Set<String> getLiteralMarkers() {
		return literalMarkers;
	}

	public String getProperty(String key) {
		return props.getProperty(key, "");
	}

	public Map<String, List<Object>> getResponseMap() {
		return responseMap;
	}

	public Map<String, String> getSimpleTextMarkerMap() {
		return simpleTextMarkerMap;
	}

}
