package ru.magnusdev.daunbot.utils;

import java.io.File;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate4.encryptor.HibernatePBEEncryptorRegistry;

public class HibernateSessionFactory {

	private volatile static HibernateSessionFactory instance;

	private static Object monitor = new Object();

	private final static String PASSWORD = "HITRIYDAUN";

	public static HibernateSessionFactory getInstance() {
		if (instance == null) {
			synchronized (monitor) {
				if (instance == null) {
					instance = new HibernateSessionFactory();
				}
			}
		}
		return instance;

	}

	private final SessionFactory sessionFactory;

	private HibernateSessionFactory() {
		StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
		strongEncryptor.setPassword(PASSWORD);
		strongEncryptor.setAlgorithm("PBEWITHMD5ANDDES");
		HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
		registry.registerPBEStringEncryptor("strongHibernateStringEncryptor", strongEncryptor);
		File cfg = new File("config/hibernate.cfg.xml");
		Configuration configuration = new Configuration().configure(cfg);
		configuration.setProperty("hibernate.connection.password",
				strongEncryptor.decrypt(configuration.getProperty("hibernate.connection.password")));
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
		sessionFactory = configuration.buildSessionFactory(builder.build());
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
