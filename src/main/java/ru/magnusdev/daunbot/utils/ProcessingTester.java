package ru.magnusdev.daunbot.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

public class ProcessingTester {

	public static void main(String[] args) {
		try {
			String spellCheckerClassName = Context.getInstance().getProperty("input.spellchecker");
			String normalizerClassName = Context.getInstance().getProperty("input.normalizer");
			String synonymizerClassName = Context.getInstance().getProperty("input.synonymizer");
			String textPreprocessorExpr = Context.getInstance().getProperty("input.escape.expr");
			TextProcessor textProcessor = new TextProcessor(spellCheckerClassName, normalizerClassName,
					synonymizerClassName, textPreprocessorExpr);
			File file = new File(args[0]);
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				while ((line = br.readLine()) != null) {
					Map<String, Collection<String>> result = textProcessor.process(line);
					for (Entry<String, Collection<String>> entity : result.entrySet()) {
						System.out.println("Initial word - " + entity.getKey());
						System.out.print("Possible word forms: ");
						for (String str : entity.getValue()) {
							System.out.print("[" + str + "] ");
						}
						System.out.println();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
