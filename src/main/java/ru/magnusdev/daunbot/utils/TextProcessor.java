package ru.magnusdev.daunbot.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import ru.magnusdev.daunbot.normalizer.NormalizationAdapter;
import ru.magnusdev.daunbot.normalizer.NormalizerPool;
import ru.magnusdev.daunbot.spellchecker.SpellCheckerAdapter;
import ru.magnusdev.daunbot.spellchecker.SpellCheckerPool;
import ru.magnusdev.daunbot.synonymizer.SynonymizerAdapter;
import ru.magnusdev.daunbot.synonymizer.SynonymizerPool;

public class TextProcessor {

	private NormalizerPool normalizerPool;
	private SpellCheckerPool spellCheckerPool;
	private SynonymizerPool synonymizerPool;
	private String textPreprocessorExpr;

	@SuppressWarnings("unchecked")
	public TextProcessor(	String spellCheckerClassName, String normalizerClassName, String synonimizerClassName,
							String textPreprocessorExpr)
		throws ClassNotFoundException {
		Class<SpellCheckerAdapter> spellCheckerClazz = (Class<SpellCheckerAdapter>) Class
				.forName(spellCheckerClassName);
		Class<SynonymizerAdapter> synonymizerClazz =
				(Class<SynonymizerAdapter>) Class.forName(synonimizerClassName);
		Class<NormalizationAdapter> normalizerClazz = (Class<NormalizationAdapter>) Class.forName(normalizerClassName);
		spellCheckerPool = new SpellCheckerPool(spellCheckerClazz);
		normalizerPool = new NormalizerPool(normalizerClazz);
		synonymizerPool = new SynonymizerPool(synonymizerClazz);
		this.textPreprocessorExpr = textPreprocessorExpr;
	}

	public Map<String, Collection<String>> process(String text) throws Exception {
		String[] words = text.replaceAll(textPreprocessorExpr, " ").replaceAll("\\s+", " ").split(" ");
		Map<String, Collection<String>> result = spellCheckerPool.checkWords(Arrays.asList(words));
		for (String entry : result.keySet()) {
			Map<String, Collection<String>> forms = normalizerPool.normalizeWords(result.get(entry));
			result.get(entry).clear();
			for (Entry<String, Collection<String>> wordList : forms.entrySet()) {
				result.get(entry).addAll(wordList.getValue());
			}
		}
		for (String entry : result.keySet()) {
			Map<String, Collection<String>> forms =
					synonymizerPool.synonymizeWords(result.get(entry));
			result.get(entry).clear();
			for (Entry<String, Collection<String>> wordList : forms.entrySet()) {
				result.get(entry).addAll(wordList.getValue());
			}
		}
		return result;
	}

}
